import './polyfillImports';
import React, { useRef } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Route, Switch, HashRouter } from 'react-router-dom';
import { Hub } from '@aws-amplify/core';
import Login from 'pages/Login/Login';
import { createHashHistory } from 'history';
import AppRoot from './App.js';
import PrivateRoute from 'services/checkAuth';
import { configureAuth } from 'services/aws_services';

import * as serviceWorker from './serviceWorker';
import 'semantic-ui-css/semantic.min.css'
import configureStore from './redux-state/store/configureStore'


//use react hooks to validate authentication
const history = createHashHistory();
const store = configureStore()

const Application = (props) => {

    //Init Authentication
    configureAuth();


    // The Amplify Listener Signs-out or Signs-in the user
    // This would also work with Cognito Hosted UI

    Hub.listen('auth', (data) => {
        const { payload } = data;
        switch (payload.event) {                        
            case 'signOut':
                    props.history.push('/');            
                break;
            default:                
                break;
        }
    });


    return (
        <div>
            <Provider store={store.store}>
                <HashRouter>
                    <Switch>
                        <Route exact path="/" component={Login} />
                        <PrivateRoute component={AppRoot} />
                    </Switch>
                </HashRouter>
            </Provider>
        </div>
    )
}

ReactDOM.render(<Application history={history} />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
