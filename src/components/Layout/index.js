import React, { Component } from "react";
import { Link, HashRouter, Switch, Route } from "react-router-dom";
import { connect } from 'react-redux'
import MenuConfig from 'config/menuConfig'
import Loading from 'components/Loading/Loading';
import Retry from 'components/Loading/LoaderRetry';
import ModuleLoader from 'services/splitLoader';
import { Icon as IconAnt } from 'antd'
import { userLogout } from 'redux-state/modules/logout/actions';
import { signOut } from 'services/aws_services';


import {
    Grid,
    Icon,
    Menu,
    Segment,
    MenuItem,
    Sidebar,
    Dropdown
} from "semantic-ui-react";

import './layout.css'

const themeColor = 'teal'
const projectName = 'Admin Panel'
function Loader(props) {
    if (props.error) {
        return <Retry message='Error! Please Click to Retry' retry={props.retry} />;
    } else if (props.timedOut) {
        return <Retry message='Loading..... Taking a long time?' retry={props.retry} />;
    } else if (props.pastDelay) {
        return <Loading />;
    } else {
        return null;
    }
}

const Home = ModuleLoader({
    loader: () => import('pages/Home'),
    loading: Loader
});

const Bookings = ModuleLoader({
    loader: () => import('pages/Bookings'),
    loading: Loader
});

const BookingDetails = ModuleLoader({
    loader: () => import('pages/Bookings/components/bookingDetails'),
    loading: Loader
});

const Customers = ModuleLoader({
    loader: () => import('pages/Customers'),
    loading: Loader
});

const Walkers = ModuleLoader({
    loader: () => import('pages/Walkers'),
    loading: Loader
});

const Products = ModuleLoader({
    loader: () => import('pages/Products'),
    loading: Loader
});

const WalkerCustomerDetails = ModuleLoader({
    loader: () => import('pages/WalkerCustomerDetails'),
    loading: Loader
});

class AppLayout extends Component {


    constructor(props) {
        super(props)
        this.state = {
            dropdownMenuStyle: {
                display: "none"
            },
            activeItem: "Bookings",
        };
    }

    handleItemClick = (e, { name }) => this.setState({ activeItem: name });
    getCurrentActiveRoute = () => {
        console.log(this.props)
        const { currentLocation } = this.props;
        var activeRoute = currentLocation.pathname.substr(1);
        return activeRoute.charAt(0).toUpperCase() + activeRoute.slice(1)
    }

    componentDidMount() {
      this.setState({activeItem: this.getCurrentActiveRoute()})
    }

    handleToggleDropdownMenu = () => {
        let newState = Object.assign({}, this.state);
        if (newState.dropdownMenuStyle.display === "none") {
            newState.dropdownMenuStyle = { display: "flex", backgroundColor: 'transparent', boxShadow: '2px 0 6px rgba(0, 21, 41, .35)' };
        } else {
            newState.dropdownMenuStyle = { display: "none" };
        }

        this.setState(newState);
    };

    createMenuItem = () => {
        const { activeItem } = this.state;
        const menu = MenuConfig.menu.map((menuItem) => {
            return (
                <Menu.Item as={Link} to={menuItem.path} name={`${menuItem.name}`} active={activeItem === menuItem.name}
                    key={menuItem.name} onClick={this.handleItemClick}>

                    <Icon name={menuItem.icon} />
                    {menuItem.name}
                </Menu.Item>
            )
        })
        return menu
    }

    // static getDerivedStateFromProps(props, state) {
    //     // Any time the current user changes,
    //     // Reset any parts of state that are tied to that user.
    //     // In this simple example, that's just the email.
    //      console.log(props)
    //     if (props.userID !== state.prevPropsUserID) {
    //     return {
    //         prevPropsUserID: props.userID,
    //         email: props.defaultEmail
    //     };
    //     }
    //     return null;
    // }
    mobileOnlyMenu = () => {
        return (
            <Grid padded className="mobile only">
                <Menu borderless color={themeColor} inverted size='wide' fixed="top">
                    <Menu.Item>
                        <Icon onClick={this.handleToggleDropdownMenu} fitted flipped="vertically" name="content" />
                    </Menu.Item>
                    {/* <Menu.Item header >
                        {projectName}
                    </Menu.Item> */}
                    <Menu.Item header >
                        {
                            this.state.activeItem
                        }
                    </Menu.Item>
                    <Menu.Menu position="right">
                        <Menu.Item>
                            {
                                this.profileItem()
                            }
                        </Menu.Item>


                    </Menu.Menu>
                    <Menu
                        stackable
                        fluid
                        style={this.state.dropdownMenuStyle}
                    >
                        {
                            this.createMenuItem()
                        }
                    </Menu>
                </Menu>
            </Grid>

        )
    }


    sideBar = () => {
        return (
            <Sidebar
                as={Menu}
                animation='push'
                icon='labeled'
                inverted
                color="teal"
                // onHide={() => setVisible(false)}
                vertical
                visible={this.state.dropdownMenuStyle.display === 'flex'}
                width='thin'
            >
                <Menu.Item as='a'>
                    <Icon name='home' />
                    Home
            </Menu.Item>
                <Menu.Item as='a'>
                    <Icon name='gamepad' />
                    Games
            </Menu.Item>
                <Menu.Item as='a'>
                    <Icon name='camera' />
                    Channels
            </Menu.Item>
            </Sidebar>
        )
    }

    profileItem = () => {
        return (
            <Dropdown floating item labeled icon="user" text="vijay chavre" style={{ marginRight: '0px', fontSize: '14px' }}>
                <Dropdown.Menu>
                    <Dropdown.Item id="profile-item" onClick={this.handleLogout}>
                        <IconAnt type="logout" style={{ marginRight: '5px' }} />
                        <label >Logout</label>
                    </Dropdown.Item>
                    <Dropdown.Item id="profile-item" >
                        <Icon name="dashboard"></Icon>
                        <a href="https://app.dashbird.io/lambdas/ap-south-1/collrs-adminpanel-getproducts-get/requests/5e61e05cf24ab39fb397bb1e0f0075885ee1e84779e6" target="_blank"><label style={{ color: 'black' }}>Dashbird</label></a>
                    </Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        )
    }

    logoItem = () => {
        return (
            <Menu.Item >
                <div id="app-logo">{projectName}</div>
            </Menu.Item>
        )
    }

    handleLogout = async (e) => {
        try {
            await signOut();
            this.props.logout();
            // this.props.history.push(`/app/login`);
        } catch (err) {
            console.log('Unable to logout. Please try again later.');
        }
    };

    render() {

        return (
            <HashRouter>
                <div className="App">
                    <Grid padded className="tablet computer only">
                        <Menu size='massive' borderless color={themeColor} inverted fluid fixed="top" id="topbar">
                            <Menu.Item fluid id="">
                                {
                                    this.state.activeItem
                                }
                            </Menu.Item>
                            <Menu.Menu position="right">
                                {
                                    this.profileItem()
                                }
                            </Menu.Menu>
                        </Menu>

                    </Grid>
                    {
                        this.mobileOnlyMenu()
                    }
                    <Grid padded>
                        <Grid.Column
                            tablet={2}
                            computer={2}
                            only="tablet computer"
                            id="sidebar"
                            style={{ backgroundColor: themeColor }}
                        >
                            <Menu vertical color={themeColor} inverted fluid text>
                                {
                                    this.logoItem()
                                }
                                {
                                    this.createMenuItem()
                                }
                            </Menu>

                        </Grid.Column>

                        <Grid.Column
                            mobile={16}
                            tablet={14}
                            computer={14}
                            floated="right"
                            id="content"
                        //onClick={this.handleToggleDropdownMenu}
                        >
                            {/* {
                                this.sideBar()
                            } */}

                            <Switch>
                                <Route exact path="/bookings" component={Bookings} />
                                <Route exact path="/booking/details/:bookingId" component={BookingDetails} />
                                <Route exact path="/customers" component={Customers} />
                                <Route exact path="/walkers" component={Walkers} />
                                <Route exact path="/products" component={Products} />
                                <Route exact path="/walkercustomerdetails/:id" component={WalkerCustomerDetails} />
                            </Switch>
                        </Grid.Column>
                    </Grid>
                </div>
            </HashRouter>
        );
    }
}


const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(userLogout())
    };
}

export default connect(mapDispatchToProps)(AppLayout);