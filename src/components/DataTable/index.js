import React from 'react'
import _ from 'lodash'
import { Icon, Table, Tab, Segment } from 'semantic-ui-react'

const defaultTextAlingn = 'center'

const setHeader = (columns) => {
    return (
        <Table.Header>
            <Table.Row>
                {
                    columns.map((col) => {
                        return (
                            <Table.HeaderCell textAlign={col.textAlign || defaultTextAlingn}>{col.name}</Table.HeaderCell>
                        )
                    })
                }

            </Table.Row>
        </Table.Header>

    )
}

const setTableRow = (dataSource, columns, loading) => {
    let rows = []
    if (dataSource && dataSource.length > 0) {
        rows = dataSource.map((data, i) => {
            return (
                <Table.Row key={`row-${i}`}>
                    {
                        columns.map((col, i) => {
                            return col.render ? <Table.Cell key={`cell-${i}`} textAlign={col.textAlign || defaultTextAlingn}>{col.render(data[col.dataIndex], data)}</Table.Cell> : <Table.Cell>{data[col.dataIndex]}</Table.Cell>
                        })
                    }
                </Table.Row>
            )
        })


        return rows
    }
    return <Table.Row textAlign="center"><Table.HeaderCell colSpan={columns.length} ><Segment padded secondary loading={loading} basic>No Data</Segment></Table.HeaderCell></Table.Row>
}
const DataTable = (props) => {
    return (
        <div style={{overflowX: 'scroll'}}>
        <Table size="large" unstackable>
            {
                setHeader(props.columns)
            }
            <Table.Body>
                {
                    setTableRow(props.dataSource, props.columns, props.loading)
                }
            </Table.Body>
        </Table>
        </div>
    )
}


DataTable.defaulProps = {
    columns: [],
    dataSource: [],
    loading: false
}

export default DataTable