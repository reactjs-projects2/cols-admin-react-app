import React, { Component } from "react";

import { Segment, Grid, Menu } from 'semantic-ui-react'


const PageLayout = (props) => {
    return(
        <React.Fragment>
            <Segment size="large" padded raised>Home</Segment>
            <Grid padded>
                <Grid.Column style={{ height: '100tvh'}}>
                    {props.children}
                    <Segment textAlign="center" secondary fluid basic>
              
                            <span style={{textAlign: 'center'}}>
                            Footer
                            </span>  
               
                    </Segment>
                </Grid.Column>
            </Grid>
           
        </React.Fragment>
    )
}

export default PageLayout