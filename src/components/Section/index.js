import React from 'react'
import { Divider, Header, Segment } from 'semantic-ui-react'

const Section = (props) => {
    return (
        <React.Fragment>
            <Segment>
                {
                    (props.title && props.title !== '') && (<div><Header as="h3" style={{ fontWeight: '400' }}>{props.title}</Header> <Divider /></div>)
                }
                <Segment padded basic>
                    {
                        props.children
                    }
                </Segment>

            </Segment>
        </React.Fragment>
    )
}

export default Section