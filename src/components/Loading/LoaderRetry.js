import React from 'react';
import { Button } from 'semantic-ui-react';

export default function LoaderRetry(props) {
  return (
        <div style={{
            width: '100%',
            height: '100%',
            margin: 'auto',
            paddingTop: 50,
            textAlign: 'center',
            }}>
            <span>{props.message}</span><br/>         
            <Button content='Retry' onClick={props.retry} />
        </div>
  )
}
