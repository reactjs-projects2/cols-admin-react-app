import React from 'react';
import { Loader } from "semantic-ui-react";

export default function Loading() {
    return (
        <div style={{
                    width: '100%',
                    height: '100%',
                    margin: 'auto',
                    paddingTop: 50,
                    textAlign: 'center',
                    }}>
            <Loader />
        </div>
    ); 
}
