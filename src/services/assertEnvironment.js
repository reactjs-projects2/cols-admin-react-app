
const getAmplifyConfig = () => {
    let AmplifyConfig;
    
    switch (process.env.REACT_APP_HOST_TYPE) {
        case 'nonprod':            
            AmplifyConfig = './configGlobal/authConfig.nonprod.json';
            break;
        case 'prod':            
            AmplifyConfig = './configGlobal/authConfig.prod.json';            
            break;
        default:            
            AmplifyConfig = './configGlobal/authConfig.dev.json';
            break;
    }
    return require(`${AmplifyConfig}`);
}

module.exports = {
    getAmplifyConfig
};