import { Signer } from '@aws-amplify/core';
import Auth from '@aws-amplify/auth';
import axios from 'axios';

const lambdaRequest = async (funcName, payload) => {
    return new Promise(async(resolve, reject) => {
        const cred = await Auth.currentCredentials();
        const user = await Auth.currentAuthenticatedUser();
        // console.log('Current User', user.signInUserSession.idToken.payload)
        const { accessKeyId, secretAccessKey, sessionToken } = Auth.essentialCredentials(cred);
        const accessInfo = {
            access_key: accessKeyId,
            secret_key: secretAccessKey,
            session_token: sessionToken
        };
        const service_info = {
            region: 'ap-south-1',
            service: 'lambda',
        };
        const request = {
            method: "POST",
            url: `https://lambda.ap-south-1.amazonaws.com/2015-03-31/functions/${funcName}/invocations`,
            data: JSON.stringify(payload)
        };

        const signedRequest = Signer.sign(request, accessInfo, service_info); try {
            const result = await axios({
                ...signedRequest
            });
            console.log("Signed Request RESPONSE *************: ", JSON.parse(result.data.body));
            resolve(JSON.parse(result.data.body))
        } catch (err) {
            console.log("###########  AXIOS ERROR #############", err);
            reject(err)
        }
    })
}

export {
    lambdaRequest
}