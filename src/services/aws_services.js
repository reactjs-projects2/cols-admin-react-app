import Amplify from '@aws-amplify/core';
import Auth from '@aws-amplify/auth';
import Storage from '@aws-amplify/storage';
import { loadData, removeData } from 'services/localstorage';
import assertEnv from 'services/assertEnvironment';

export function configureAuth(force = false) {
    const AuthConfig = assertEnv.getAmplifyConfig();
     console.log(AuthConfig.auth)
    Amplify.configure(
        {
            Auth: AuthConfig.auth,
        }
    );
}

/**
 * This function helps configure S3 bucket for the Amplify Storage module singleton on demand
 * @param {String} bucket - Name of the S3 bucket
 * @param {String} level - Optional Protection level public, private or protected (defaults to protected)
 * @param {String} region - Optional AWS Region for the bucket (defaults to us-east-1)
 */
export function SetS3Config(bucket, level, region, identityPool) {
    const AuthConfig = assertEnv.getAmplifyConfig();
    Storage.configure({
        bucket: bucket,
        level: (level !== undefined ? level : 'protected'),
        region: (region !== undefined ? region : 'us-east-1'),
        identityPoolId: (identityPool !== undefined)? identityPool : AuthConfig.auth.identityPoolId
    });
}

/**
 * Async function to Sign a user in using AWS Amplify and Cognito.
 * @param {String} username - The username
 * @param {String} password - The password
 * @return Returns a promise
 */
export async function signIn(username, password) {
    try {
        const user = await Auth.signIn(username, password);
        return user;
    } catch (err) {
        throw err;
    }
}

/**
 * Async function to send challenge response to Cognito for authentication
 * @param {CognitoUser} user 
 * @param {String} challenge 
 */
export async function sendCustomChallenge(user, challenge) {
    try {
        const retuser = Auth.sendCustomChallengeAnswer(user, challenge);
        return retuser;
    } catch (err) {
        throw err;
    }
}

/**
 * Async function to Confirm user using temporary password from invite.
 * @param {String} username - The username
 * @param {String} password - The password from invitation email
 * @param {String} new_password - The new password
 * @return Returns a promise
 */
export async function inviteSignIn(username, password, new_password) {
    try {
        const user = await Auth.signIn(username, password);
        await Auth.completeNewPassword(user, new_password);
        await Auth.updateUserAttributes(user, {
            'custom:invite_accepted': 'Y'
        });
        return true;
    } catch (err) {
        throw err;
    }
}

/**
 * Async function to get the current logged in user
 * @return Returns a promise resolving to session isvalid, the session object, cognito user
 */
export async function getUserSession() {
    try {
        const user = await Auth.currentAuthenticatedUser();
        return {
            isAuthenticated: user.signInUserSession.isValid(),
            session: user.signInUserSession,
            user
        };
    } catch (err) {
        throw err;
    }
}

/**
 * Async function to get the user's identity ID
 * @return Returns a promise resolving to the user's identity id
 */
export async function getIdentityId() {
    try {
        const creds = await Auth.currentCredentials();
        return creds.identityId;
    } catch (err) {
        throw err;
    }
}

/**
 * Asyn function to log out.
 * @return Returns a promise resolving to successful logout
 */
export async function signOut() {
    try {
        await Auth.signOut();
        return true;
    } catch (err) {
        throw err;
    }
}

/**
 * Async function to request reset password
 * @param {String} username - The username to change password
 * @return Returns a promise
 */
export async function requestResetPassword(username) {
    try {
        const data = await Auth.forgotPassword(username);
        return data;
    } catch (err) {
        throw err;
    }
}



/**
 * Confirm a new password using confirmation code
 * @param {String} username 
 * @param {String} vcode 
 * @param {String} newpassword 
 * @return Returns a promist
 */
export async function confirmResetPassword(username, vcode, newpassword) {
    try {
        const data = await Auth.forgotPasswordSubmit(username, vcode, newpassword);
        return data;
    } catch (err) {
        throw err;
    }
}


/**
 * Change current password with new password
 * @param {Object} user 
 * @param {String} currentPassword 
 * @param {String} newPassword 
 * @return Returns a promist
 */

export async function changePassword(user, currentPassword, newPassword) {
    try {
        let result = await Auth.changePassword(user, currentPassword, newPassword);
        if (result) {
            return result
        }
    } catch (err) {
       // err.message = 'Password cannot be changed at this time'
        throw err;
    }
}

