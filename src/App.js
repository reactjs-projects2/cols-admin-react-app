import React from 'react';
import Layout from './components/Layout';
import { HashRouter } from 'react-router-dom'
import './App.css'
function App(props) {
  return (
    <HashRouter basename="/app">
        <Layout currentLocation={props.location}/>
    </HashRouter>

  );
}

export default App;
