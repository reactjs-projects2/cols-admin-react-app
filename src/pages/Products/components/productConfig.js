
import React from 'react'
import { Link } from 'react-router-dom'
import { Date } from 'core-js'
const productConfig = (context) => {
    const columns = [
        {
            name:'Product Id',
            dataIndex:'PRODUCT_ID',
            render:(value, record) => {
            return <Link onClick={() => {context.getProductUpc(record)}}><span><strong>{record.PRODUCT_ID}</strong></span></Link>
            }
        },
        {
            name:'Product Name',
            dataIndex:'PRODUCT_NAME',
            render:(value, record) => {
            return <span>{value}</span>
            }
        },
        {
            name:'Description',
            dataIndex:'PRODUCT_DESC',
            render:(value, record) => {
            return <span>{value}</span>
            }
        },
        {
            name:'Sub Description',
            dataIndex:'PRODUCT_SUB_DESC',
            render:(value, record) => {
            return <span>{value}</span>
            }
        },
        {
            name:'Duration',
            dataIndex:'DURATION',
            render:(value, record) => {
            return <span>{value}</span>
            }
        },
        {
            name:'Units',
            dataIndex:'DURATION_UNIT',
            render:(value, record) => {
            return <span>{value}</span>
            }
        },
        {
            name:'Marketing Description',
            dataIndex:'MARKETING_URL',
            render:(value, record) => {
            return <span>{value}</span>
            }
        }

    ];

    const upcColumns = [
        {
            name:'UPC ID',
            dataIndex:'UPC_ID',
            render:(value, record) => {
            return <span>{value || 'n/a'}</span>
            }
        },
        {
            name:'Description',
            dataIndex:'UPC_DESCRIPTION',
            render:(value, record) => {
            return <span>{value || 'n/a'}</span>
            }
        },
        {
            name:'Frequency',
            dataIndex:'FREQUENCY',
            render:(value, record) => {
            return <span>{value || 'n/a'}</span>
            }
        },
        {
            name:'Units',
            dataIndex:'FREQUENCY_UNITS',
            render:(value, record) => {
            return <span>{value || 'n/a'}</span>
            }
        },
        {
            name:'Price',
            dataIndex:'PRICE',
            render:(value, record) => {
            return <span>{value || 'n/a'}</span>
            }
        },
        {
            name:'Offer',
            dataIndex:'OFFER',
            render:(value, record) => {
            return <span>{value || 'n/a'}</span>
            }
        },
        {
            name:'Conditions',
            dataIndex:'CONDITIONS',
            render:(value, record) => {
            return <span>{value || 'n/a'}</span>
            }
        }
    ]

    return {
        columns,
        upcColumns
    }
}
 export {
    productConfig
}