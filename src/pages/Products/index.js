import React from 'react';
import { Segment, Header, Image, Responsive, Form } from "semantic-ui-react";
import PageLayout from 'components/PageLayout'
import Section from 'components/Section'
import DataTable from 'components/DataTable'
import { productConfig } from './components/productConfig'
import { getProducts } from 'redux-state/api/productAPI'

class Products extends React.Component {

    constructor(props) {
        super(props)
        const { columns, upcColumns } = productConfig(this)
        this.state = {
            data: [],
            columns: columns,
            upcColumns: upcColumns,
            selectedProduct: undefined,
            upcData: [],
            loadingProducts: false,
            loadingUpc: false
        }
    }
    getProductData = async () => {
        try {

            const payload = {
                action: 'list',
            }
            this.setState({ loadingProducts: true })
            const res = await getProducts(payload)
            if (res.responseCode === 200) {
                this.setState({ data: [...res.data], loadingProducts: false })
            }

        } catch (err) {
            console.log(err)
            this.setState({ loadingProducts: false })

        }
    }

    getProductUpc = async (record) => {

        const payload = {
            action: 'details',
            productId: record.PRODUCT_ID
        }
        try {
            this.setState({ selectedProduct: record, loadingUpc: true })
            const res = await getProducts(payload)
            if (res.responseCode === 200) {
                this.setState({ selectedProduct: record, loadingUpc: false, upcData: [...res.data] })
            }

        } catch (err) {
            console.log(err)
            this.setState({ loadingUpc: false })
        }
    }

    componentDidMount() {
        this.getProductData()
    }

    getValue = (value) => {
        return <span>{value ? value : 'n/a'}</span>
    }

    render() {
        const { data, columns, selectedProduct, upcColumns, upcData, loadingUpc, loadingProducts } = this.state
        return (
            <PageLayout>
                <Section title="Products">
                    <Responsive minWidth="2">
                        <DataTable
                            columns={columns}
                            dataSource={data}
                            loading={loadingProducts}
                        />
                    </Responsive>
                </Section>
                <Form id="details-form" >
                    <Section>
                        <Segment>
                            <Segment style={{ maxWidth: '100%' }} basic >
                                <Form.Group widths="equal" >
                                    <Form.Field inline >
                                        <label>Product Id : </label>
                                        {this.getValue(selectedProduct && selectedProduct.PRODUCT_ID)}
                                    </Form.Field>
                                    <Form.Field inline >
                                        <label> Sub Description: </label>
                                        {this.getValue(selectedProduct && selectedProduct.PRODUCT_SUB_SESC)}
                                    </Form.Field>
                                    <Form.Field inline >
                                        <label> Marketing Message: </label>
                                        {this.getValue(selectedProduct && selectedProduct.MARKETING_SLUG)}
                                    </Form.Field>
                                </Form.Group>
                                <Form.Group widths="equal" >
                                    <Form.Field inline >
                                        <label>Product Name : </label>
                                        {this.getValue(selectedProduct && selectedProduct.PRODUCT_NAME)}
                                    </Form.Field>
                                    <Form.Field inline >
                                        <label>Duration : </label>
                                        {this.getValue(selectedProduct && selectedProduct.DURATION)}
                                    </Form.Field>
                                    <Form.Field inline >
                                        {/* <label>Marketing Image : 
                                        {selectedProduct && selectedProduct.MARKETING_IMAGE ? 
                                            <Image src={selectedProduct.MARKETING_IMAGE} size='tiny' circular spaced/> : 
                                            <Image src='https://react.semantic-ui.com/images/wireframe/square-image.png' size='mini' circular spaced/>
                                        }</label> */}
                                    </Form.Field>
                                </Form.Group>
                                <Form.Group widths="equal" >
                                    <Form.Field inline >
                                        <label>Description : </label>
                                        {this.getValue(selectedProduct && selectedProduct.PRODUCT_DESC)}
                                    </Form.Field>
                                    <Form.Field inline >
                                        <label>Duration Units : </label>
                                        {this.getValue(selectedProduct && selectedProduct.DURATION_UNIT)}
                                    </Form.Field>
                                    <Form.Field inline >
                                        {/* <label>Marketing Image : 
                                        {selectedProduct && selectedProduct.MARKETING_IMAGE ? 
                                            <Image src={selectedProduct.MARKETING_IMAGE} size='tiny' circular spaced/> : 
                                            <Image src='https://react.semantic-ui.com/images/wireframe/square-image.png' size='mini' circular spaced/>
                                        }</label> */}
                                    </Form.Field>
                                </Form.Group>
                                <Form.Group widths="equal" >
                                    <Form.Field inline >
                                        <label>Marketing Image :
                                        {selectedProduct && selectedProduct.MARKETING_IMAGE ?
                                                <Image src={selectedProduct.MARKETING_IMAGE} size='tiny' circular spaced /> :
                                                <Image src='https://react.semantic-ui.com/images/wireframe/square-image.png' size='mini' circular spaced />
                                            }</label>
                                    </Form.Field>
                                </Form.Group>
                            </Segment>
                            <Segment basic>
                                <DataTable
                                    columns={upcColumns}
                                    dataSource={upcData}
                                    loading={loadingUpc}
                                />
                            </Segment>
                        </Segment>

                    </Section>
                </Form>

            </PageLayout>
        )
    }
}

export default Products