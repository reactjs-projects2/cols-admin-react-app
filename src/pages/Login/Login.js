import React from 'react'
import { Redirect } from 'react-router-dom'
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react'
import { Hub } from '@aws-amplify/core';
//import Loading from 'components/Loading/Loading';

import Auth from '@aws-amplify/auth';
import { getUserSession, signIn } from 'services/aws_services';



class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            loggedIn: false,
            username: '',
            password: '',
            newPassword: '',
            isSetNewPassword: false,
            err: undefined,
            isError: false,
            loading: false,
            isChecking: true
        }
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    handleLogin = async () => {
        const { username, password, newPassword } = this.state
        try {
            this.setState({ loading: true })
            const user = await signIn(username, password)
            if (user.challengeName === 'NEW_PASSWORD_REQUIRED') {
                const { requiredAttributes } = user.challengeParam;

                console.log("required attributes", requiredAttributes)
                // the array of required attributes, e.g ['email', 'phone_number']
                // You need to get the new password and required attributes from the UI inputs
                // and then trigger the following function with a button click
                // For example, the email and phone_number are required attributes
                // For example, the email and phone_number are required attributes
                // const {username, email, phone_number} = getInfoFromUserInput();

                if (newPassword === '') {
                    this.setState({ isSetNewPassword: true, loggedIn: false, loading: false })
                } else {
                    const loggedUser = await Auth.completeNewPassword(
                        user,              // the Cognito User Object
                        newPassword,       // the new password
                        // OPTIONAL, the required attributes
                        {
                            email: username,

                        }
                    );
                    console.log('new password complted', loggedUser)
                    this.setState({ loggedIn: true, isSetNewPassword: false, isError: false, loading: false })
                }

            } else {
                console.log(user)
                this.setState({ loggedIn: true, isError: false, loading: false })
            }

        } catch (err) {
            console.log(err)
            this.setState({ err, isError: true, loading: false })
        }
    }
    LoginForm = () => {
        const { username, password, newPassword, isSetNewPassword } = this.state

        return (
            <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign="middle">
                <Grid.Column style={{ maxWidth: 450, }}>
                    <Segment secondary loading={this.state.loading} raised>
                        <Header as='h2' color='teal' textAlign='center'>
                            <Image src='/logo192.png' /> Log-in to your account
                     </Header>
                        <Form error={this.state.isError}>
                            <Segment stacked size="large">
                                <Form.Input
                                    fluid icon='user'
                                    iconPosition='left'
                                    placeholder='E-mail address'
                                    name='username'
                                    value={username}
                                    onChange={this.handleChange}
                                />
                                <Form.Input
                                    fluid
                                    icon='lock'
                                    iconPosition='left'
                                    placeholder='Password'
                                    type='password'
                                    name='password'
                                    value={password}
                                    onChange={this.handleChange}
                                />

                                {
                                    isSetNewPassword && <Form.Input
                                        fluid
                                        icon='lock'
                                        iconPosition='left'
                                        placeholder=' New Password'
                                        type='newPassword'
                                        name='newPassword'
                                        value={newPassword}
                                        onChange={this.handleChange}
                                    />
                                }

                                <Message
                                    size="small"
                                    error
                                    content={this.state.isError && this.state.err.message}
                                />

                                <Button color="teal" fluid size='large' onClick={this.handleLogin}>
                                    Login
                        </Button>
                            </Segment>
                        </Form>
                    </Segment>
                    {/* <Message>
                        New to us? <a href='#'>Sign Up</a>
                    </Message> */}
                </Grid.Column>
            </Grid>
        )
    }

    initiateLoginListener = () => {
        Hub.listen('auth', (data) => {
            const { payload } = data;
            switch (payload.event) {
                case 'signIn':
                    if (this._isMounted) {
                        this.setState({ isChecking: false, loggedIn: true });
                    }
                    break;
                case 'signIn_failure':
                    if (this.isFederatedCallaback.ssoClient) {
                        this.setState({ isChecking: false, loggedIn: false, SAMLerr: this.isFederatedCallaback.message });
                    }
                    break;
                // case 'cognitoHostedUI':          
                //     props.history.push('/app/home');   
                //     break;
                default:
                    break;
            }
        });
    }

    async componentDidMount() {
        // this.initiateLoginListener();
        // this.setState({ isChecking: true }); 
        this._isMounted = true;
        try {
            const res = await getUserSession();
            if (!this.state.loggedIn) {
                this.setState({ isChecking: false, loggedIn: res.isAuthenticated });
            }
        } catch (err) {
            this.initiateLoginListener();
            console.log(err)
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
        Hub.remove('auth');
    }

    render() {
        // if (this.state.isChecking && !this.state.loggedIn) {
        //     return <Loading />;
        // }

        if (this.state.loggedIn) {
            return (
                <Redirect to="/bookings" />
            );
        }
        return (
            this.LoginForm()
        )
    }
}

export default Login