import React, { useState } from 'react';
import { Segment, Header, Form, Divider, Input, Button, Responsive } from "semantic-ui-react";
import PageLayout from 'components/PageLayout'
import DataTable from 'components/DataTable'
import Section from 'components/Section'
//import BookingCard from './components/bookingCard'
import { customerConfig } from './components/customerConfig'
import { getCustomers } from 'redux-state/api/customerAPI'

class Customer extends React.Component {
    constructor(props) {
        super(props)
        const { columns } = customerConfig(this)
        this.state = {
            data: [],
            columns: columns,
            loadingCustomers: false,
            customer_name: '',
            customer_email: '',
            customer_phone: '',
            searchingCustomer: false
        }
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    handleSubmit = () => {
       this.setState({searchingCustomer: true})
       this.getCustomerData()
    }


    getCustomerData = async () => {
        const { customer_email, customer_name, customer_phone } = this.state;
        try {

            const payload = {
                action: 'list',
                customer_name, 
                customer_email,
                customer_phone
            }
            this.setState({ loadingCustomers: true })
            const res = await getCustomers(payload)
            if (res.responseCode === 200) {
                this.setState({ data: [...res.data], loadingCustomers: false, searchingCustomer: false })
            }

        } catch (err) {
            console.log(err)
            this.setState({ loadingCustomers: false, searchingCustomer: false })

        }
    }

    componentDidMount() {
        this.getCustomerData()
    }
    render() {
        const { columns , data, loadingCustomers, customer_email, customer_name, customer_phone, searchingCustomer } = this.state;
        return (
            <PageLayout>
                <Segment>
                    <Header as='h3'>Search Customer</Header>
                    <Divider />
                    <Segment padded basic loading={searchingCustomer}>
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Group widths="2">
                                <Form.Field
                                    id='form-input-control-first-name'
                                    control={Input}
                                    label='Name'
                                    placeholder='Enter your Name'
                                    name='customer_name'
                                    value={customer_name}
                                    onChange={this.handleChange}
                                />
                                <Form.Field
                                    id='form-input-control-last-name'
                                    control={Input}
                                    label='Customer Email'
                                    placeholder="Enter customer's email id"
                                    name='customer_email'
                                    value={customer_email}
                                    onChange={this.handleChange}
                                />
                                <Form.Field
                                    id='form-input-control-last-name'
                                    control={Input}
                                    label='Phone'
                                    placeholder="Enter customer's mobile number"
                                    name='customer_phone'
                                    value={customer_phone}
                                    onChange={this.handleChange}
                                />
                            </Form.Group>

                            <Form.Field
                                id='form-button-control-public'
                                control={Button}
                                content='Search'
                            />
                        </Form>
                    </Segment>
                </Segment>
                <Section title="Customer">
                    {/* <Responsive onUpdate={(e, { width }) => setWidth(width)} maxWidth="767">
                        <BookingCard bookingData={data} width={width} />
                    </Responsive> */}
                    <Responsive  minWidth="2">
                        <DataTable 
                        columns={columns} 
                        dataSource={data}
                        loading={loadingCustomers}
                        />
                    </Responsive>
                </Section>
            </PageLayout>
        )
    }
   
};

export default Customer