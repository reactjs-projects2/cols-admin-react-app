
import React from 'react'
import { Link } from 'react-router-dom'

const detailConfig = (context) => {
    const columns = [
        {
            name:'Booking Id',
            dataIndex:'BOOKING_ID',
            render:(value, record) => {
            return <span><strong>{record.BOOKING_ID}</strong></span>
            }
        },
        {
            name:'Booking Date',
            dataIndex:'BOOKING_DATE',
            render:(value, record) => {
            return <span>{value}</span>
            }
        },
        {
            name:'Walk Frequency',
            dataIndex:'FREQUENCY',
            render:(value, record) => {
            return <span>{value}</span>
            }
        },
        {
            name:'Product',
            dataIndex:'PRODUCT_ID',
            render:(value, record) => {
            return <span>{value}</span>
            }
        },
        {
            name:'Price',
            dataIndex:'PRICE',
            render:(value, record) => {
            return <span>{value}</span>
            }
        }
    ];

  
    return {
        columns
    }
}
export {
    detailConfig
}