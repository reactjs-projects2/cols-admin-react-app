import React from 'react';
import { Segment, Header, Image, Responsive, Form } from "semantic-ui-react";
import PageLayout from 'components/PageLayout'
import Section from 'components/Section'
import DataTable from 'components/DataTable'
import { detailConfig } from './components/detailConfig'
import { getWalkerCustomerDetails } from 'redux-state/api/walkerCustomerDetailAPI'

class WalkerCustomerDetails extends React.Component {

    constructor(props) {
        super(props)
        const { columns} = detailConfig(this)
        this.state = {
            data: [],
            columns: columns,
            loadingDetails: false,
            walkerCustomerId: props.match.params.id.split('-')[1],
            isWalker:  props.match.params.id.split('-')[0] === 'walker',
            userDetail: undefined
        }
        console.log(this.state)
    }
    getDetails = async () => {
        try {

            const payload = {
                action: 'details',
                type: this.state.isWalker ? 'walker' : 'customer',
                user_id: this.state.walkerCustomerId
            }
            this.setState({ loadingDetails: true })
            const res = await getWalkerCustomerDetails(payload)
            if (res.responseCode === 200) {
                this.setState({ data: [...res.data.bookings], userDetail:res.data.userDetails, loadingDetails: false })
            }

        } catch (err) {
            console.log(err)
            this.setState({ loadingDetails: false })

        }
    }

    componentDidMount() {
     this.getDetails()
    }

    getValue = (value) => {
        return <span>{value ? value : 'n/a'}</span>
    }

    render() {
        const { data, columns, loadingDetails, userDetail } = this.state
        return (
            <PageLayout>
                <Form id="details-form" >
                    <Section>
                        <Segment>
                            <Segment style={{ maxWidth: '100%' }} basic >
                                <Form.Group widths="equal" >
                                    <Form.Field inline >
                                        <label>Name : </label>
                                        {this.getValue(userDetail && userDetail.FIRST_NAME)}
                                    </Form.Field>
                                    <Form.Field inline >
                                        <label> Email: </label>
                                        {this.getValue(userDetail && userDetail.USER_EMAIL)}
                                    </Form.Field>
                                    <Form.Field inline >
                                        <label> Status: </label>
                                        {this.getValue(userDetail && userDetail.STATUS)}
                                    </Form.Field>
                                </Form.Group>
                                <Form.Group widths="equal" >
                                    <Form.Field inline >
                                        <label>Phone : </label>
                                        {this.getValue(userDetail && userDetail.PHONE_NUMBER)}
                                    </Form.Field>
                                    <Form.Field inline >
                                        <label>Address : </label>
                                        {this.getValue(userDetail && userDetail.ADDRESS)}
                                    </Form.Field>
                                    <Form.Field inline >
                                        <label>Join Date : </label>
                                        {this.getValue(userDetail && userDetail.CREATE_DATE)}
                                    </Form.Field>
                                </Form.Group>
                            </Segment>
                            <Segment basic>
                                <DataTable
                                    columns={columns}
                                    dataSource={data}
                                    loading={loadingDetails}
                                />
                            </Segment>
                        </Segment>

                    </Section>
                </Form>

            </PageLayout>
        )
    }
}

export default WalkerCustomerDetails