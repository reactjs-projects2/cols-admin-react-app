import React from 'react';
import { Segment, Header, Image } from "semantic-ui-react";
import PageLayout from 'components/PageLayout'

const Home = props => (
    <PageLayout>
        <Segment>
            <Header as='h3'>Home</Header>
        </Segment>
    </PageLayout>
);

export default Home