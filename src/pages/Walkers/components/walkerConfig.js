

import React from 'react'
import { Link } from 'react-router-dom'

const walkerConfig = (context) => {
    const columns = [
        {
            name:'Walker Id',
            dataIndex:'USER_ID',
            render:(value, record) => {
            return <Link to={`/walkercustomerDetails/walker-${value}`}><span><strong>{record.USER_ID}</strong></span></Link>
            }
        },

        {
            name:'Email',
            dataIndex:'USER_EMAIL',
            render:(value, record) => {
            return <span>{value}</span>
            }
        },
        {
            name:'Address',
            dataIndex:'ADDRESS',
            render:(value, record) => {
            return <span>{value || 'n/a'}</span>
            }
        },
        {
            name:'Pin',
            dataIndex:'PIN_CODE',
            render:(value, record) => {
            return <span>{value }</span>
            }
        },
        {
            name:'Phone',
            dataIndex:'PHONE_NUMBER',
            render:(value, record) => {
            return <span>{value}</span>
            }
        },
        {
            name:'Status',
            dataIndex:'STATUS',
            render:(value, record) => {
            return <span>{value}</span>
            }
        }
    ];

    return {
        columns,
    }
}
export {
    walkerConfig
}