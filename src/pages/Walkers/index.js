import React, { useState } from 'react';
import { Segment, Header, Form, Divider, Input, Button, Responsive } from "semantic-ui-react";
import PageLayout from 'components/PageLayout'
import DataTable from 'components/DataTable'
import Section from 'components/Section'
import { walkerConfig } from './components/walkerConfig'
import { getWalkers } from 'redux-state/api/walkerAPI'

class Walker extends React.Component {
    constructor(props) {
        super(props)
        const { columns } = walkerConfig(this)
        this.state = {
            data: [],
            columns: columns,
            loadingWalkers: false,
            walker_name: '',
            walker_email: '',
            walker_phone: '',
            searchingWalker: false
        }
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    handleSubmit = () => {
    this.setState({searchingWalker: true})
    this.getWalkerData()
    }


    getWalkerData = async () => {
        const { walker_email, walker_name, walker_phone } = this.state;
        try {

            const payload = {
                action: 'list',
                walker_name, 
                walker_email,
                walker_phone
            }
            this.setState({ loadingWalkers: true })
            const res = await getWalkers(payload)
            if (res.responseCode === 200) {
                this.setState({ data: [...res.data], loadingWalkers: false, searchingWalker: false })
            }

        } catch (err) {
            console.log(err)
            this.setState({ loadingWalkers: false, searchingWalker: false })

        }
    }

    componentDidMount() {
        this.getWalkerData()
    }
    render() {
        const { columns , data, loadingWalkers, walker_email, walker_name, walker_phone, searchingWalker } = this.state;
        return (
            <PageLayout>
                <Segment>
                    <Header as='h3'>Search Walker</Header>
                    <Divider />
                    <Segment padded basic loading={searchingWalker}>
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Group widths="2">
                                <Form.Field
                                    id='form-input-control-first-name'
                                    control={Input}
                                    label='Name'
                                    placeholder='Enter your Name'
                                    name='walker_name'
                                    value={walker_name}
                                    onChange={this.handleChange}
                                />
                                <Form.Field
                                    id='form-input-control-last-name'
                                    control={Input}
                                    label='Walker Email'
                                    placeholder="Enter Walker's email id"
                                    name='walker_email'
                                    value={walker_email}
                                    onChange={this.handleChange}
                                />
                                <Form.Field
                                    id='form-input-control-last-name'
                                    control={Input}
                                    label='Phone'
                                    placeholder="Enter Walker's mobile number"
                                    name='walker_phone'
                                    value={walker_phone}
                                    onChange={this.handleChange}
                                />
                            </Form.Group>

                            <Form.Field
                                id='form-button-control-public'
                                control={Button}
                                content='Search'
                            />
                        </Form>
                    </Segment>
                </Segment>
                <Section title="Walker">
                    {/* <Responsive onUpdate={(e, { width }) => setWidth(width)} maxWidth="767">
                        <BookingCard bookingData={data} width={width} />
                    </Responsive> */}
                    <Responsive  minWidth="2">
                        <DataTable 
                        columns={columns} 
                        dataSource={data}
                        loading={loadingWalkers}
                        />
                    </Responsive>
                </Section>
            </PageLayout>
        )
    }
   
};

export default Walker