import React, { useState } from 'react';
import { Segment, Header, Form, Divider, Input, Button, Responsive } from "semantic-ui-react";
import PageLayout from 'components/PageLayout'
import DataTable from 'components/DataTable'
import Section from 'components/Section'
//import BookingCard from './components/bookingCard'
import { columns, data } from './components/bookingConfig'
import { getUserSession } from 'services/aws_services';
import { getBookings } from 'redux-state/api/bookingAPI'


const Bookings = props => {
    // const [width, setWidth] = useState(window.innerWidth)
  const  invokeLambda = async () => {
        try {
           const res = await getBookings()
            console.log(res)
        } catch (err) {
            console.log(err)
        }
    }

    invokeLambda()
    return (
        <PageLayout>
            <Segment>
                <Header as='h3'>Search Bookings</Header>
                <Divider />
                <Segment padded basic> 
                    <Form >
                        <Form.Group widths="4">
                            <Form.Field
                                id='form-input-control-first-name'
                                control={Input}
                                label='Booking Id'
                                placeholder='Enter your booking id'
                            />
                            <Form.Field
                                id='form-input-control-last-name'
                                control={Input}
                                label='Customer Email'
                                placeholder="Enter customer's email id"
                            />
                        </Form.Group>

                        <Form.Field
                            id='form-button-control-public'
                            control={Button}
                            content='Search'

                        />
                    </Form>
                </Segment>
            </Segment>
            <Section title="Bookings">
                {/* <Responsive onUpdate={(e, { width }) => setWidth(width)} maxWidth="767">
                    <BookingCard bookingData={data} width={width} />
                </Responsive> */}
                <Responsive  minWidth="2">
                    <DataTable 
                    columns={columns} 
                    dataSource={data}
                    />
                </Responsive>
            </Section>
        </PageLayout>
    )
};

export default Bookings