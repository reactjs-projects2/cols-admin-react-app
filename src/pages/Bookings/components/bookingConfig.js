
import React from 'react'
import { Link } from 'react-router-dom'
import { Date } from 'core-js'
const columns = [
    {
        name:'Booking Id',
        dataIndex:'booking_id',
        render:(value, record) => {
         return <Link to={`/booking/details/${value}`}><span><strong>{record.booking_id}</strong></span></Link>
        }
    },

    {
        name:'Booking Date',
        dataIndex:'booking_date',
        render:(value, record) => {
        return <span>{value.toDateString()}</span>
        }
    },
    {
        name:'Walk Frequency',
        dataIndex:'walker_frequency',
        render:(value, record) => {
        return <span>{value}</span>
        }
    },
    {
        name:'Product',
        dataIndex:'product',
        render:(value, record) => {
        return <span>{value}</span>
        }
    },
    {
        name:'Price',
        dataIndex:'price',
        render:(value, record) => {
        return <span>{value}</span>
        }
    }
];


const data = [
    {
        booking_id:'1',
        booking_date: new Date(),
        walker_frequency: 6,
        product: 'P1',
        price: 20,
    },
    {
        booking_id:'2',
        booking_date: new Date(),
        walker_frequency: 6,
        product: 'P1',
        price: 20,
    }
]
 export {
    columns,
    data
}