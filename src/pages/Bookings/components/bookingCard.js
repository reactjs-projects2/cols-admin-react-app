import React from 'react'
import { Card } from 'semantic-ui-react'
import './card.css'
const bookingCard = (props) => {


    const getCards = () => {
        return (
            props.bookingData.map((data) => {
                return <Card fluid id="booking-card">
                    <Card.Content style={{ backgroundColor: 'teal' }}>
                        <Card.Header textAlign="center" style={{ color: '#ffff', fontSize: '20px' }}>{data.booking_id}</Card.Header>
                    </Card.Content>
                    <Card.Content textAlign="left">
                        <Card.Meta>Booking date: {data.booking_date.toDateString()}</Card.Meta>
                        <Card.Meta> Walker Frequency: {data.walker_frequency}</Card.Meta>
                        <Card.Meta>product: {data.product}</Card.Meta>
                        <Card.Meta>price: ${data.product}</Card.Meta>

                    </Card.Content>
                </Card>
            })
        )
    }
    return (
        <Card.Group itemsPerRow={props.width <= 600 ? 1 : 2} centered >
            {getCards()}
        </Card.Group>
    )
}

export default bookingCard;