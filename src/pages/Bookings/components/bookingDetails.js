import React from 'react'
import { Grid, Image, Form, Input, Segment, Icon } from 'semantic-ui-react'
import { DatePicker } from 'antd'
import PageLayout from 'components/PageLayout'
import Section from 'components/Section'
import './card.css'
class BookingDetails extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            productId: '',
            bookingStartDate: '',
            boookingStatus:'',
            walkFrequency: '',
            paymentAmount: '',
            totalWalks:0,
            walksCompleted:'',
            assignedWalkerId:'',
            meetGreetDate: '',
            walkerApproved:'',
        }
    }
    sectionOne = () => {
        return (
            <Section>
                <Segment >
                    <Segment style={{ maxWidth: '90%' }} basic>
                        <Form.Group widths="equal" >
                            <Form.Field inline >
                                <label>Booking ID : </label>
                                <span >Id-1</span>
                            </Form.Field>

                            <Form.Field
                                inline={true}
                                label="Booking start Date"
                                // error={{ content: 'errororo', pointing: 'above' }}

                                onChange={(date, dateString) => {
                                    console.log(date, dateString)
                                }}
                                control={DatePicker}
                                size="large"
                            />
                            <Form.Field inline>
                                <label>Booking End Date</label>
                                <span>02/02/2020</span>
                            </Form.Field>
                        </Form.Group>

                        <Form.Group widths="equal">
                            <Form.Field inline>
                                <label>Product ID : </label>
                                <Input placeholder='(xxx)' />
                            </Form.Field>
                            <Form.Field inline>
                                <label>Product Description</label>
                                <span>Product description</span>
                            </Form.Field>
                            <Form.Field inline>
                                <label>Booking Status</label>
                                <Input placeholder='(xxx)' />
                            </Form.Field>
                        </Form.Group>
                        <Form.Group widths="equal">
                            <Form.Field inline >
                                <label>Walk Frequency : </label>
                                <span >Id-1</span>
                            </Form.Field>
                            <Form.Field inline>
                                <label>Instructions</label>
                                <Input placeholder='(xxx)' />
                            </Form.Field>
                            <Form.Field inline>
                                <label>Payment Status</label>
                                <Input placeholder='(xxx)' />
                            </Form.Field>
                        </Form.Group>
                    </Segment>
                </Segment>
            </Section>

        )
    }

    sectionTwo = () => {
        return (
            <Section>
                <Segment >
                    <Segment style={{ maxWidth: '90%' }} basic >
                        <Form.Group widths="equal" >
                            <Form.Field inline >
                                <label>Total Number of Walks : </label>
                                <Input placeholder='(xxx)' />
                            </Form.Field>
                            <Form.Field inline>
                                <label>Walks Completed</label>
                                <Input placeholder='(xxx)' />
                            </Form.Field>
                        </Form.Group>
                    </Segment>
                </Segment>
            </Section>
        )
    }

    sectionThree = () => {
        return (
            <Section>
                <Segment>
                    <Segment style={{ maxWidth: '90%' }} basic >
                        <Form.Group widths="equal" >
                            <Form.Field inline >
                                <label>Customer Name : </label>
                                <span >Id-1</span>
                            </Form.Field>
                            <Form.Field inline >
                                <label>Customer Address: </label>
                                <span >Id-1</span>
                            </Form.Field>
                        </Form.Group>
                        <Form.Group widths="equal" >
                            <Form.Field inline >
                                <label>Customer Email : </label>
                                <span >Id-1</span>
                            </Form.Field>
                            <Form.Field inline >
                                <label>Customer Phone : </label>
                                <span >Id-1</span>
                            </Form.Field>
                        </Form.Group>
                    </Segment>
                </Segment>
            </Section>


        )
    }

    sectionFour = () => {
        return (
            <Section>
                <Segment >
                    <Segment style={{ maxWidth: '90%' }} basic>
                        <Form.Group widths="equal" >
                            <Form.Field inline >
                                <label>Assigned Walker ID: </label>
                                <Input placeholder='(xxx)' />
                            </Form.Field>
                            <Form.Field
                                inline
                                label="Meet & Greet Date"
                                onChange={(date, dateString) => {
                                    console.log(date, dateString)
                                }}
                                control={DatePicker}
                                size="large"
                            />
                        </Form.Group>
                        <Form.Group widths="equal" >
                            <Form.Field inline >
                                <label>Assigned Walker Name: </label>
                                <span>Walker Name</span>
                            </Form.Field>
                            <Form.Field inline>
                                <label>Meet & Greet Date</label>
                                <Input placeholder='(xxx)' />
                            </Form.Field>
                        </Form.Group>
                    </Segment>
                </Segment>
            </Section>

        )
    }
    render() {
        return (
            <PageLayout>
                <Form size="massive" id="details-form">
                    {
                        this.sectionOne()
                    }
                    {
                        this.sectionTwo()
                    }
                    {
                        this.sectionThree()
                    }
                    {
                        this.sectionFour()
                    }
                </Form>
            </PageLayout>
        )
    }
}
export default BookingDetails