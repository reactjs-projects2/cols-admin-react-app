
const menu = [
    {
        name:'Bookings',
        path:'/bookings',
        icon:'paste'
    },
    {
        name:'Customers',
        path:'/customers',
        icon:'users'
    },
    {
        name:'Walkers',
        path:'/walkers',
        icon:'user'
    },
    {
        name:'Products',
        path:'/products',
        icon:'shopping bag'
    },
]



export default {
    menu
}