import { combineReducers } from 'redux'
import Menu from './menu/reducer'
import {USER_LOGOUT} from './logout/actions';
/**
 *  Combine reducers
 */
const appReducer = combineReducers({
  Menu,
});

const rootReducer = (state, action) => {    
  if (action.type === USER_LOGOUT) {    
      state = undefined;
  }

  return appReducer(state, action);
}

export default rootReducer;