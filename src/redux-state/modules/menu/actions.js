const menuActions = {

    /* Menu Actions */
    FETCH_MENU_COLLAPSE : 'fetch_menu_collapse',
    
    setCollapse: (collapsed) => ({
      type: menuActions.FETCH_MENU_COLLAPSE,
      payload: {
        collapsed
      }
    })
    
  };
  
  export default menuActions
  