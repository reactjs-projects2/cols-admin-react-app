

import menuActions from './actions';

const initialState = {
    collapsed: true
}

export default function menuReducer(state = initialState, action) {
    switch (action.type) {
        case menuActions.FETCH_MENU_COLLAPSE:
            return Object.assign({}, state, {
                collapsed: action.payload.collapsed
            });
        default:
            return state;
    }

}