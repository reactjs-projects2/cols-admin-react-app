export const USER_LOGOUT = 'user_logout';

export function userLogout(){
   return dispatch => {
    dispatch({
        type: USER_LOGOUT
    });
   }
}