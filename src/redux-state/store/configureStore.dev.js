import { applyMiddleware, createStore } from 'redux'
import thunkMiddleware from 'redux-thunk'
import promise from 'redux-promise';
import { composeWithDevTools } from 'redux-devtools-extension'
import loggerMiddleware from '../middleware/logger'
import rootReducer from '../modules/rootReducer'
/*
  Configure Store - Development
*/
export default function configureStore(initialState) {

  const middlewares = [thunkMiddleware, promise]
  // const middlewares = [thunkMiddleware, promise]
  const middlewareEnhancer = applyMiddleware(...middlewares)

  // #region createStore : enhancer
  const enhancers = [middlewareEnhancer]
  
  const composedEnhancers = composeWithDevTools(...enhancers)
  const store = createStore(rootReducer, initialState, composedEnhancers)

  return { store }

}