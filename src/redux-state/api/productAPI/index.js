import { lambdaRequest } from 'services/aws_lambda_service'
const getProducts = async (payload) => {
    return new Promise(async(resolve, reject) => {
        try {
            const result = await lambdaRequest('collrs-adminpanel-getproducts-get', payload)
            resolve(result)
        } catch (err) {
            console.log("###########  AXIOS ERROR #############", err);
            reject(err)
        }
    })
}

export {
    getProducts
}