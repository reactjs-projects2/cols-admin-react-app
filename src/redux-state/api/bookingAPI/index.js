import { lambdaRequest } from 'services/aws_lambda_service'
const getBookings = async (payload) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await lambdaRequest('collrs-adminpanel-getbookings-get', payload)
            resolve(result)
        } catch (err) {
            console.log("###########  AXIOS ERROR #############", err);
            reject(err)
        }
    })
}

export {
    getBookings
}