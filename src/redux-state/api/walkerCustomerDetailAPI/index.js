import { lambdaRequest } from 'services/aws_lambda_service'
const getWalkerCustomerDetails = async (payload) => {
    return new Promise(async(resolve, reject) => {
        try {
            const result = await lambdaRequest('collrs-adminpanel-walkercustomerdetail-get', payload)
            resolve(result)
        } catch (err) {
            console.log("###########  AXIOS ERROR #############", err);
            reject(err)
        }
    })
}

export {
    getWalkerCustomerDetails
}